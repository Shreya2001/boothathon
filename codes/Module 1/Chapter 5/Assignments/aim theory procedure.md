> ## Aim
**To implement 1 Bit Full Adder using Multiplexer. It is a digital system experiment where a user can test circuits. Availability of a Digital Electronics virtual lab can immensely compliment the laboratory component of the courseware as it allows harmless experimentation during the learning process of the topic. The experiment setup allows the student to choose from a collection of Basic and Universal Gates to implement combitional circuits – in this case 1 Bit Full Adder.**

>## Theory
***An adder is digital circuit that perform addition of numbers. In modern computer adder resides in the arithmetic logic unit (ALU).***
There are two types of Adder:

i)Half Adder

ii)Full Adder
### **Half Adder**
Half Adders are the most basic of the adders. The half adder accepts two binary digits on its inputs and produce two binary digits outputs, a sum bit and a carry bit.

![](dia_1.png)

Figure 1(a) - Logic Diagram of a HalfAdder

|INPUT|| OUTPUT||
|:----|:----:|:----:|----:|
|A|B|S|C|
|0|0|0|0|
|1|0|1|0|
|0|1|1|0|
|1|1|0|1|
Figure 1(b) - Truth Table of a HalfAdder

A half-adder can also be realized in universal logic by using either only NAND gates or only NOR gates as shown in Figure 2 and Figure 3.

![](dia_2.png)

Figure 3 - Logic Diagram of a HalfAdder using only two NAND Gates

The sum (S) bit and the carry (C) bit (Figure 1, 2 and 3), according to the rules of binary addition, are given by: The sum (S) is the X-OR of A and B (it represents the LSB of the sum). Therefore, S = AB̅ + A̅B = AB The carry (C) is the AND of A and B (it is 0 unless both the inputs are 1). 

Now, ***S = AB̅ + A̅B = AB̅ + AA̅ + A̅B + BB̅ = A(A̅+B̅) + B(A̅+B̅) = A + B = C = AB Therefore, C = AB***


>## Procedure
1. Choose the IC to use in the experiment – place the component on the breadboard.

2. Connect the inputs of the IC to the logic sources (A, B) and its output to the logic indicators (S, C) – make the wire connections between the various components.

3. Apply various input combinations and observe output for each one – use the DIP switches to apply various input combinations and note the LED indicators.

4. Verify the truth table for each input/output combination.

5. Exit the experiment when done – use the “Close” button to exit.