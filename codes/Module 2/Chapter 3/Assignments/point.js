var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var t5 = document.getElementById("t5");
var t6 = document.getElementById("t6");
var t7 = document.getElementById("t7");
var t8 = document.getElementById("t8");
function check() {
    var x1 = +t1.value;
    var y1 = +t2.value;
    var x2 = +t3.value;
    var y2 = +t4.value;
    var x3 = +t5.value;
    var y3 = +t6.value;
    var x = +t7.value;
    var y = +t8.value;
    //to check wheter a string is entered or not
    if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(x) || isNaN(y1) || isNaN(y2) || isNaN(y3) || isNaN(x) || isNaN(y)) {
        alert("INVALID INFORMATION!!!");
    }
    else {
        var area;
        var a1;
        var a2;
        var a3;
        var sum;
        var absol;
        var s1;
        var s2;
        var s3;
        //lenghts of sides of triangle
        s1 = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
        s2 = Math.sqrt(Math.pow((x2 - x3), 2) + Math.pow((y2 - y3), 2));
        s3 = Math.sqrt(Math.pow((x3 - x1), 2) + Math.pow((y3 - y1), 2));
        //forms a triangle or not
        if ((s1 + s2) > s3 && (s2 + s3 > s1) && (s1 + s3 > s2)) {
            //area of main triangle
            area = (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2;
            //areas of triangle formed by the point with any 2 vertices
            a1 = Math.abs((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1)) / 2);
            a2 = Math.abs((x * (y1 - y3) + x1 * (y3 - y) + x3 * (y - y1)) / 2);
            a3 = Math.abs((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2);
            sum = a1 + a2 + a3;
            console.log(a1);
            console.log(a2);
            console.log(a3);
            //console.log(Math.abs(sum));
            //console.log(Math.abs(area));
            //logic
            if (Math.abs(area) == Math.abs(sum)) {
                alert("The given point lies inside the triangle");
            }
            else {
                alert("The given point lies outside the triangle");
            }
        }
        //if sides does not form a triangle    
        else {
            alert("The given points do not form a triangle");
        }
    }
}
